package  com.spring.microservices.db.entity;


import java.sql.ResultSet;
import java.sql.SQLException;

public class AppAdvUser {
	private String email;
	private String password;
	private String type;
	private String cf;
	private String name;
	private String surname;
	private String piva;
	private String ndg;
	private String phone;
	private boolean emailConf;
	private boolean smsConf;
	private Long validityDateFrom;
	private Long validityDateTo;
	private Long insertDate;
	private String insertUser;
	private Long updateDate;
	private String updateUser;
	private boolean disabled;
	private boolean deleted;
		
	public AppAdvUser() {}
	
	public AppAdvUser(ResultSet sqlResult) throws SQLException {
		this.email = sqlResult.getString("EMAIL");
		this.password = sqlResult.getString("PASSWORD");
		this.type = sqlResult.getString("TYPE");
		this.cf = sqlResult.getString("CF");
		this.name = sqlResult.getString("NAME");
		this.surname = sqlResult.getString("SURNAME");
		this.piva = sqlResult.getString("PIVA");
		this.ndg = sqlResult.getString("NDG");
		this.phone = sqlResult.getString("PHONE");
		this.emailConf = sqlResult.getBoolean("EMAIL_CONF");
		this.smsConf = sqlResult.getBoolean("SMS_CONF");
		if(sqlResult.getDate("VALIDITY_DATE_FROM") != null){this.validityDateFrom = sqlResult.getDate("VALIDITY_DATE_FROM").getTime();}
		if(sqlResult.getDate("VALIDITY_DATE_TO") != null){this.validityDateTo = sqlResult.getDate("VALIDITY_DATE_TO").getTime();}
		if(sqlResult.getDate("INSERT_DATE") != null){this.insertDate = sqlResult.getDate("INSERT_DATE").getTime();}
		this.insertUser = sqlResult.getString("INSERT_USER");
		if(sqlResult.getDate("UPDATE_DATE") != null){this.updateDate = sqlResult.getDate("UPDATE_DATE").getTime();}
		this.updateUser = sqlResult.getString("UPDATE_USER");
		this.disabled = sqlResult.getBoolean("DISABLED");
		this.deleted = sqlResult.getBoolean("DELETED");
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPiva() {
		return piva;
	}

	public void setPiva(String piva) {
		this.piva = piva;
	}

	public String getNdg() {
		return ndg;
	}

	public void setNdg(String ndg) {
		this.ndg = ndg;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isEmailConf() {
		return emailConf;
	}

	public void setEmailConf(boolean emailConf) {
		this.emailConf = emailConf;
	}

	public boolean isSmsConf() {
		return smsConf;
	}

	public void setSmsConf(boolean smsConf) {
		this.smsConf = smsConf;
	}

	public Long getValidityDateFrom() {
		return validityDateFrom;
	}

	public void setValidityDateFrom(Long validityDateFrom) {
		this.validityDateFrom = validityDateFrom;
	}

	public Long getValidityDateTo() {
		return validityDateTo;
	}

	public void setValidityDateTo(Long validityDateTo) {
		this.validityDateTo = validityDateTo;
	}

	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	
}



