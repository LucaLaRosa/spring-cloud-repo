package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.spring.microservices.db.pojo.MetadataAppProductCatalogPojo;
//import com.alba.pv.utils.AdvancedLogger;
import com.google.gson.Gson;

public class AppProductCatalog {
	private String title;
	private String description;
	private Long idAppVendor;
	private Long id;
	private Long idParent;
	private Long validityDateFrom;
	private Long validityDateTo;
	private Long insertDate;
	private String insertUser;
	private Long updateDate;
	private String updateUser;
	private boolean disabled;
	private boolean deleted;
	private Double prezzoUnitario;
	private String link;
	private MetadataAppProductCatalogPojo metadata;
	private int foglia;
	
	//transient
	private List<AppProductCatalog> listChildren;
	private String parentTitle;
	private String parentDescription;
	private int counterInsideCart;
	private List<String> breadcrumbInfos = new ArrayList<String>();
		
	public AppProductCatalog() {}
	
	public AppProductCatalog(ResultSet sqlResult) throws SQLException {
		this.title = sqlResult.getString("TITLE");
		this.description = sqlResult.getString("DESCRIPTION");
		this.idAppVendor = sqlResult.getLong("ID_APP_VENDOR");
		this.id = sqlResult.getLong("ID");
		
		this.idParent = sqlResult.getLong("ID_PARENT");
		if(sqlResult.wasNull()){this.idParent = null;}
		
		if(sqlResult.getDate("VALIDITY_DATE_FROM") != null){this.validityDateFrom = sqlResult.getDate("VALIDITY_DATE_FROM").getTime();}
		if(sqlResult.getDate("VALIDITY_DATE_TO") != null){this.validityDateTo = sqlResult.getDate("VALIDITY_DATE_TO").getTime();}
		if(sqlResult.getDate("INSERT_DATE") != null){this.insertDate = sqlResult.getDate("INSERT_DATE").getTime();}
		this.insertUser = sqlResult.getString("INSERT_USER");
		if(sqlResult.getDate("UPDATE_DATE") != null){this.updateDate = sqlResult.getDate("UPDATE_DATE").getTime();}
		this.updateUser = sqlResult.getString("UPDATE_USER");
		this.disabled = sqlResult.getBoolean("DISABLED");
		this.deleted = sqlResult.getBoolean("DELETED");
		this.prezzoUnitario = sqlResult.getDouble("PREZZO_UNITARIO");
		this.link = sqlResult.getString("LINK");
		this.metadata = new Gson().fromJson(sqlResult.getString("METADATA"), MetadataAppProductCatalogPojo.class);
		this.foglia = sqlResult.getInt("FOGLIA");
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getIdAppVendor() {
		return idAppVendor;
	}

	public void setIdAppVendor(Long idAppVendor) {
		this.idAppVendor = idAppVendor;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdParent() {
		return idParent;
	}

	public void setIdParent(Long idParent) {
		this.idParent = idParent;
	}

	public Long getValidityDateFrom() {
		return validityDateFrom;
	}

	public void setValidityDateFrom(Long validityDateFrom) {
		this.validityDateFrom = validityDateFrom;
	}

	public Long getValidityDateTo() {
		return validityDateTo;
	}

	public void setValidityDateTo(Long validityDateTo) {
		this.validityDateTo = validityDateTo;
	}

	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public MetadataAppProductCatalogPojo getMetadata() {
		return metadata;
	}

	public void setMetadata(MetadataAppProductCatalogPojo metadata) {
		this.metadata = metadata;
	}

	public List<AppProductCatalog> getListChildren() {
		return listChildren;
	}

	public void setListChildren(List<AppProductCatalog> listChildren) {
		this.listChildren = listChildren;
	}

	public String getParentTitle() {
		return parentTitle;
	}

	public void setParentTitle(String parentTitle) {
		this.parentTitle = parentTitle;
	}

	public String getParentDescription() {
		return parentDescription;
	}

	public void setParentDescription(String parentDescription) {
		this.parentDescription = parentDescription;
	}

	public int getCounterInsideCart() {
		return counterInsideCart;
	}

	public void setCounterInsideCart(int counterInsideCart) {
		this.counterInsideCart = counterInsideCart;
	}

	public Double getPrezzoUnitario() {
		return prezzoUnitario;
	}

	public void setPrezzoUnitario(Double prezzoUnitario) {
		this.prezzoUnitario = prezzoUnitario;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public List<String> getBreadcrumbInfos() {
		return breadcrumbInfos;
	}

	public void setBreadcrumbInfos(List<String> breadcrumbInfos) {
		this.breadcrumbInfos = breadcrumbInfos;
	}

	public int getFoglia() {
		return foglia;
	}

	public void setFoglia(int foglia) {
		this.foglia = foglia;
	}

	
	
	
}
