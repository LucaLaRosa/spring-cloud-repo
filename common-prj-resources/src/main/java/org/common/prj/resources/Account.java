package org.common.prj.resources;

public class Account {

	private Integer id;
	private Integer customerId;
	private String number;
	private String parent;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public Account(Integer id, Integer customerId, String number) {
		super();
		this.id = id;
		this.customerId = customerId;
		this.number = number;
		this.parent = "my_mum";
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	



}
