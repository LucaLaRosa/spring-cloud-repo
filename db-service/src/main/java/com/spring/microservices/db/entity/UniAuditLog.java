package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UniAuditLog {
	private Long id;
	private String task;
	private String category;
	private String subCategory;
	private String logMessage;
	private String classMetadata;
	private String metadata;
	private String applicationName;
	private Long insertDate;
	private String insertUser;
		
	public UniAuditLog() {}
	
	public UniAuditLog(ResultSet sqlResult) {
		try {
			this.id = sqlResult.getLong("ID");
			this.task = sqlResult.getString("TASK");
			this.category = sqlResult.getString("CATEGORY");
			this.subCategory = sqlResult.getString("SUB_CATEGORY");
			this.logMessage = sqlResult.getString("LOG_MESSAGE");
			this.classMetadata = sqlResult.getString("CLASS_METADATA");
			this.metadata = sqlResult.getString("METADATA");
			this.applicationName = sqlResult.getString("APPLICATION_NAME");
			if(sqlResult.getTimestamp("INSERT_DATE") != null){this.insertDate = sqlResult.getTimestamp("INSERT_DATE").getTime();}
			this.insertUser = sqlResult.getString("INSERT_USER");
		} catch (SQLException ex) {
			Logger.getLogger(UniAuditLog.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSubCategory() {
		return subCategory;
	}

	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}

	public String getLogMessage() {
		return logMessage;
	}

	public void setLogMessage(String logMessage) {
		this.logMessage = logMessage;
	}

	public String getClassMetadata() {
		return classMetadata;
	}

	public void setClassMetadata(String classMetadata) {
		this.classMetadata = classMetadata;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}
	
}



