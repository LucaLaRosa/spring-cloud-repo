package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AppOrderDocument extends PaginationBaseEntity{
	private Long id;
	private String idOrder;
	private String uUid;
	private String path;
	private String status;
	private String message;
	private String codTypeDocument;
	private String formatFile;
	private String name;
	private Long insertDate;
	private String insertUser;
	private Long updateDate;
	private String updateUser;

	
	
	//TRANSIENT
	

	
	public AppOrderDocument(){}
	
	public AppOrderDocument(ResultSet sqlResult) throws SQLException{
		this.id = sqlResult.getLong("ID");
		this.idOrder = sqlResult.getString("ID_ORDER");
		this.uUid = sqlResult.getString("UUID");
		this.path = sqlResult.getString("PATH");
		this.status = sqlResult.getString("STATUS");
		this.message = sqlResult.getString("MESSAGE");
		this.codTypeDocument = sqlResult.getString("COD_TYPE_DOCUMENT");
		this.formatFile = sqlResult.getString("FORMAT_FILE");
		this.name = sqlResult.getString("NAME");
		if(sqlResult.getDate("INSERT_DATE") != null){this.insertDate = sqlResult.getDate("INSERT_DATE").getTime();}
		this.insertUser = sqlResult.getString("INSERT_USER");
		if(sqlResult.getDate("UPDATE_DATE") != null){this.updateDate = sqlResult.getDate("UPDATE_DATE").getTime();}
		this.updateUser = sqlResult.getString("UPDATE_USER");
	
	}

	
	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	



	public String getuUid() {
		return uUid;
	}

	public void setuUid(String uUid) {
		this.uUid = uUid;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCodTypeDocument() {
		return codTypeDocument;
	}

	public void setCodTypeDocument(String codTypeDocument) {
		this.codTypeDocument = codTypeDocument;
	}

	public String getFormatFile() {
		return formatFile;
	}

	public void setFormatFile(String formatFile) {
		this.formatFile = formatFile;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(String idOrder) {
		this.idOrder = idOrder;
	}

	
	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	

	

	
	
	
	
}
