package com.spring.microservices.customer.intercomm;

import java.util.List;

import org.common.prj.resources.Account;
import org.common.prj.resources.AppVendorUser;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


	@FeignClient("db-service")
	public interface DbClient {
		

	    @RequestMapping(method = RequestMethod.GET, value = "db/appVendorUser/{inputEmail:.+}")
	    AppVendorUser getAppVendorUserExist(@PathVariable("inputEmail") String inputEmail);
	    
	}
	
	

