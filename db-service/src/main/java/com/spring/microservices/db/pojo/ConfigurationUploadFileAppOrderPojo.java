package com.spring.microservices.db.pojo;
import java.util.List;


public class ConfigurationUploadFileAppOrderPojo {
	
	private String status;
	private String name;
	private int multiUpload;
	private boolean buttonUpload;
	private List<SingleUploadFileAppOrderPojo> files;
	
	public ConfigurationUploadFileAppOrderPojo(){}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getMultiUpload() {
		return multiUpload;
	}


	public void setMultiUpload(int multiUpload) {
		this.multiUpload = multiUpload;
	}


	public boolean isButtonUpload() {
		return buttonUpload;
	}


	public void setButtonUpload(boolean buttonUpload) {
		this.buttonUpload = buttonUpload;
	}


	public List<SingleUploadFileAppOrderPojo> getFiles() {
		return files;
	}


	public void setFiles(List<SingleUploadFileAppOrderPojo> files) {
		this.files = files;
	}
	
	
	
	
}
