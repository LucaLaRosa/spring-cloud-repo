package  com.spring.microservices.db.config;


import java.sql.*;
import java.util.logging.Logger;

import javax.naming.*;
import javax.sql.*;



public class DB_Connection {
	private static Logger logger = Logger.getLogger(DB_Connection.class.getName());

	public static Connection openConnection(){
		Connection conn = null;
		try {
			Context ctx = new InitialContext();
			DataSource ds = (DataSource)ctx.lookup("jdbc/ORACLE_DB_12C_ALBA_PV");
			conn = ds.getConnection(); 
			return conn;
		} catch (Exception e) {
			//logger.severe(new StacktraceConverter().t2str(e));
			return null;
		}
	}

	public static void closeConnection(Connection conn){
		try {
			if(conn != null){
				conn.close();
			}
		} catch (SQLException e) {
			//logger.severe(new StacktraceConverter().t2str(e));
		}
	}
}
