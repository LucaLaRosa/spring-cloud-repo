package com.spring.microservices.db.pojo;

public class ConfigAppProductCatalogPojo {

	private Double value;
	private String description;

	public ConfigAppProductCatalogPojo (){}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


}
