package com.spring.microservices.db.config;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataAccessConfiguration {
 
    @Bean(destroyMethod = "close")
    public static javax.sql.DataSource datasource() {
        DataSource ds = new DataSource();
        ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
        ds.setUrl("jdbc:oracle:thin:@ALBUTFCN.DB.ITACASERVICE.IT:1521/ALBPVS1");
        ds.setUsername("ALBA_PV");
        ds.setPassword("AcN01alB4l3s1ng10");
        ds.setInitialSize(5);
        ds.setMaxActive(10);
        ds.setMaxIdle(5);
        ds.setMinIdle(2);
        return ds;
    }
 
 
 
}