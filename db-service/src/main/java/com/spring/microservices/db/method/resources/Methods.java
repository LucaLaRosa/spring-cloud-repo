package com.spring.microservices.db.method.resources;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.lang.reflect.Type;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.logging.Logger;


import javax.servlet.http.HttpServletRequest;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.google.gson.Gson;
import com.spring.microservices.db.entity.AppPassword;
import com.spring.microservices.db.entity.AppVendorUser;
import com.spring.microservices.db.method.AppUserQueries;




public class Methods {

	//private static AdvancedLogger logger = new AdvancedLogger(AppOrderQueries.class.getName());

	public String getQuery(String query){
		try {
			Properties prop = new Properties();
			InputStream is = getClass().getResourceAsStream("/com/spring/microservices/db/method/resources/query.properties");
			prop.load(is);
			is.close();
			return (String) prop.get(query);
		} catch (Exception e) {
			//logger.severe(new StacktraceConverter().t2str(e));
			return null;
		}
	}

	public static String upp(String value){
		if(value == null){
			return value;
		} else {
			return value.toUpperCase();
		}
	}

	public static XMLGregorianCalendar getXMLgcDate(String dateString , String dateFormat){
		try {
			DateFormat format = new SimpleDateFormat(dateFormat);
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(format.parse(dateString));
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (Exception e) {
			//logger.severe(e);
		}
		return null;
	}

	/**
	 * Remove Hour, minutes, seconds and millisec from Date
	 */
	public static long cleanTime(Date date){
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone("UTC+1"));
		calendar.setTime(date);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.HOUR, 0);
		return calendar.getTime().getTime();
	}



	public static void log(String desc, Object objToJson, boolean severe){
		String jsonString = null;
		if(objToJson instanceof String){
			jsonString = (String) objToJson;
		}
		else{
			jsonString =  new Gson().toJson(objToJson);
		}

		String stringToLog = desc + ": " + jsonString;
		if(!severe){
		//	inputLogger.info(stringToLog);
		}
		else{
		//	inputLogger.severe(stringToLog);
		}
	}


	public static  String randomString(  ){
		String AB = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String ab = "abcdefghijklmnopqrstuvwxyz";
		String num = "0123456789";
		SecureRandom rnd = new SecureRandom();
		int len=6;
		StringBuilder sb = new StringBuilder( len );
		for( int i = 0; i < len; i++ )
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		sb.append( ab.charAt( rnd.nextInt(ab.length()) ) );
		sb.append( num.charAt( rnd.nextInt(num.length()) ) );

		return sb.toString();
	}

	/**
	 * Take the ip in 2 cases:
	 * 	1. First case take ip from proxy server (if the pc is under proxy)
	 *  2. Second case take normal ip (standard case)
	 * @param request
	 * @return
	 */
	public static String getClientIpAddress(HttpServletRequest request) {
		String xForwardedForHeader = request.getHeader("X-Forwarded-For");
		if (xForwardedForHeader == null) {
			return request.getRemoteAddr();
		} else {
			// As of https://en.wikipedia.org/wiki/X-Forwarded-For
			// The general format of the field is: X-Forwarded-For: client, proxy1, proxy2 ...
			// we only want the client
			return new StringTokenizer(xForwardedForHeader, ",").nextToken().trim();
		}
	}

	


	
}
