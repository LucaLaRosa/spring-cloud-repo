package com.spring.microservices.db.method.utils;

import java.sql.Date;

public class DynamicQuery {
	//campo da inserire nella query per il filtro
	public static final String DOUBLE_TYPE = "DOUBLE";
	public static final String STRING_TYPE = "STRING";
	public static final String DATE_TYPE = "DATE";	

	private String queryItemInQuery;
	private Object queryItemValue;
	private String queryItemInputType;

	/**
	 * Default is STRING
	 * @param queryItemInQuery
	 * @param queryItemValue
	 */
	public DynamicQuery(String queryItemInQuery, Object queryItemValue) {
		this.queryItemInputType = STRING_TYPE;
		this.queryItemInQuery = queryItemInQuery;
		this.queryItemValue = queryItemValue;
	}

	/**
	 * Custom filter ....example ... Double Date
	 * @param queryItemInQuery
	 * @param queryItemValue
	 */
	public DynamicQuery(String queryItemInQuery, Object queryItemValue, String queryItemInputType) {
		this.queryItemInputType = queryItemInputType;
		this.queryItemInQuery = queryItemInQuery;
		this.queryItemValue = queryItemValue;
	}

	public String getQueryItemInQuery() {
		return queryItemInQuery;
	}

	public void setQueryItemInQuery(String queryItemInQuery) {
		this.queryItemInQuery = queryItemInQuery;
	}

	public Object getQueryItemValue() {
		return queryItemValue;
	}

	public void setQueryItemValue(Object queryItemValue) {
		this.queryItemValue = queryItemValue;
	}

	public String getQueryItemInputType() {
		return queryItemInputType;
	}

	public void setQueryItemInputType(String queryItemInputType) {
		this.queryItemInputType = queryItemInputType;
	}

}
