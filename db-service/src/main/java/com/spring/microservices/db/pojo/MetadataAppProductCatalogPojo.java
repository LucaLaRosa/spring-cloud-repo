package com.spring.microservices.db.pojo;



public class MetadataAppProductCatalogPojo {
	
	private int discount;
	private AccessAppProductCatalogPojo accessory = new AccessAppProductCatalogPojo();
	private ConfigAppProductCatalogPojo configuration =  new ConfigAppProductCatalogPojo();
	private SpecServAppProductCatalogPojo specification= new SpecServAppProductCatalogPojo();
	private boolean insurancePolicy;
	private String maintenanceAssistance;
	
	public MetadataAppProductCatalogPojo(){}
	
	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public AccessAppProductCatalogPojo getAccessory() {
		return accessory;
	}

	public void setAccessory(AccessAppProductCatalogPojo accessory) {
		this.accessory = accessory;
	}

	public ConfigAppProductCatalogPojo getConfiguration() {
		return configuration;
	}

	public void setConfiguration(ConfigAppProductCatalogPojo configuration) {
		this.configuration = configuration;
	}

	public SpecServAppProductCatalogPojo getSpecification() {
		return specification;
	}

	public void setSpecification(SpecServAppProductCatalogPojo specification) {
		this.specification = specification;
	}

	public boolean isInsurancePolicy() {
		return insurancePolicy;
	}

	public void setInsurancePolicy(boolean insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}

	public String getMaintenanceAssistance() {
		return maintenanceAssistance;
	}

	public void setMaintenanceAssistance(String maintenanceAssistance) {
		this.maintenanceAssistance = maintenanceAssistance;
	}
	
	
	
	}
