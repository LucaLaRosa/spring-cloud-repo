package com.spring.microservices.db.method.utils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.spring.microservices.db.entity.AppOrder;

public class QueryUtilsAppOrder {

	public static List<DynamicQuery> createDynamicStatementByFilterAppOrder(AppOrder filter) throws ParseException {
		if(filter != null){
			List<DynamicQuery> oL = new ArrayList<DynamicQuery>();
			
//			if(filter.getNumOrder() != null
//					&& !filter.getNumOrder().trim().equals("")){
//				oL.add(new DynamicQuery(addAnd(oL.size())+" ( upper(SURNAME) LIKE upper(?) ", "%"+filter.getMergedRagSocSurname()+"%"));
//				oL.add(new DynamicQuery(" OR upper(RAG_SOC) LIKE upper(?) ) ", "%"+filter.getMergedRagSocSurname()+"%"));
//			}
//			if(filter.getMergedPivaCf() != null
//					&& !filter.getMergedPivaCf().trim().equals("")){
//				oL.add(new DynamicQuery(addAnd(oL.size())+" ( upper(PIVA) = upper(?) ", filter.getMergedPivaCf()));
//				oL.add(new DynamicQuery(" OR upper(CF) = upper(?) ) ", filter.getMergedPivaCf()));
//			}
			
			if(filter.isOrderHistory()){
				oL.add(new DynamicQuery(addAnd(oL.size())+"( PRACTICE_STATUS = ? ", ""+3));
				oL.add(new DynamicQuery(" OR PRACTICE_STATUS = ?) ", ""+4));
			}
			
			if(filter.isOrderHistory() == false){
				oL.add(new DynamicQuery(addAnd(oL.size())+"( PRACTICE_STATUS = ? ", ""+1));
				oL.add(new DynamicQuery(" OR PRACTICE_STATUS = ?) ", ""+2));
				
			}
			if(filter.getStartEmissionDate() != null
					&& !filter.getStartEmissionDate().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" EMISSION_DATE > ? ", transformStringDateToMs(filter.getStartEmissionDate(), false), DynamicQuery.DATE_TYPE));
			}
			
			if(filter.getEndEmissionDate() != null
					&& !filter.getEndEmissionDate().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" EMISSION_DATE < ? ", transformStringDateToMs(filter.getEndEmissionDate(), true), DynamicQuery.DATE_TYPE));
			}
			if(filter.getStartValueDate() != null
					&& !filter.getStartValueDate().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" VALUE_DATE > ? ", transformStringDateToMs(filter.getStartValueDate(), false), DynamicQuery.DATE_TYPE));
			}
			
			if(filter.getEndValueDate() != null
					&& !filter.getEndValueDate().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" VALUE_DATE < ? ", transformStringDateToMs(filter.getEndValueDate(), true), DynamicQuery.DATE_TYPE));
			}
			
			if(filter.getMinPrice() != null
					&& filter.getMinPrice()!=0){
				oL.add(new DynamicQuery(addAnd(oL.size())+" PURCHASE_PRICE > ? ", filter.getMinPrice(), DynamicQuery.DOUBLE_TYPE));
			}
			
			if(filter.getMaxPrice() != null
					&& filter.getMaxPrice()!=0){
				oL.add(new DynamicQuery(addAnd(oL.size())+" PURCHASE_PRICE < ? ", filter.getMaxPrice(), DynamicQuery.DOUBLE_TYPE));
			}
			
			
			if(filter.getUserAdopter() != null
					&& !filter.getUserAdopter().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" (upper(USER_ADOPTER) LIKE upper(?)) ", "%"+filter.getUserAdopter()+"%"));
			}
			
			if(filter.getPracticeStatus() != null
					&& !filter.getPracticeStatus().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" PRACTICE_STATUS LIKE ? ", "%"+filter.getPracticeStatus()+"%"));
			}
			
			if(filter.getIdOrder() != null
					&& !filter.getIdOrder().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" ID_ORDER = ? ", filter.getIdOrder()));
			}
			
			if(filter.getIdContractModule() != null
					&& !filter.getIdContractModule().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" ID_CONTRACT_MODULE = ? ", filter.getIdContractModule()));
			}
			
			return oL;
		}
		return null;
	}

	private static String addAnd(int size) {
		if(size > 0){
			return " AND ";
		}
		return "";
	}

	public static void setPreparedStatement(PreparedStatement stmt, int index ,DynamicQuery dynamicQuery) throws SQLException {
		if(DynamicQuery.STRING_TYPE.equals(dynamicQuery.getQueryItemInputType())){
			stmt.setString(index, (String)dynamicQuery.getQueryItemValue());
		} else if(DynamicQuery.DOUBLE_TYPE.equals(dynamicQuery.getQueryItemInputType())){
			stmt.setDouble(index, (Double)dynamicQuery.getQueryItemValue());
		} else if(DynamicQuery.DATE_TYPE.equals(dynamicQuery.getQueryItemInputType())){
			stmt.setTimestamp(index, new Timestamp((Long)dynamicQuery.getQueryItemValue()));
		}
	}
	
	private static long transformStringDateToMs(String input, boolean isEndDate) throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date date = sdf.parse(input);
		if(isEndDate){
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.DATE, 1);
			date = c.getTime();  // dt is now the new date
		}
		return date.getTime();
	}
	
	
}
