package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.spring.microservices.db.pojo.ConfigurationUploadFileAppOrderPojo;
import com.spring.microservices.db.pojo.MetadataAppOrderPojo;
import com.spring.microservices.db.pojo.MetadataAppProductCatalogPojo;
//import com.alba.pv.services.pojo.payment.PaymentConditionsPojo;


public class AppOrder extends PaginationBaseEntity{
	private Long id;
	private String idOrder;
	private String idContract;
	private String module;
	private String idContractModule;
	private Double purchasePrice;
	private Long emissionDate;
	private Long valueDate;
	private String practiceStatus;
	private String userAdopter;
	private String purchaseDescription;
	private String goodStatus;
	private String purchaseWarranty;
	private Long goodDeliveryDate;
	private String goodDeliveryTerms;
	private String paymentConditions;
	private Long insertDate;
	private String insertUser;
	private Long updateDate;
	private String updateUser;
	private String goodInsPlaceStreet;
	private String goodInsPlaceNumber;
	private String goodInsPlaceCity;
	private String goodInsPlaceZip;
	private String goodInsPlaceProvince;
	private String goodInsPlaceCountry;
	private String goodDelPlaceStreet;
	private String goodDelPlaceNumber;
	private String goodDelPlaceCity;
	private String goodDelPlaceZip;
	private String goodDelPlaceProvince;
	private String goodDelPlaceCountry;
	private MetadataAppOrderPojo metadataAppOrderPojo;
	private String ndgProvider;
	private String ndgCustomer;
	private String referenceOffice;
	
	//TRANSIENT
	private String startEmissionDate;
	private String endEmissionDate;
	private Double minPrice;
	private Double maxPrice;
	private String startValueDate;
	private String endValueDate;
	private boolean orderHistory;
	//private PaymentConditionsPojo paymentConditionsPojo;

	public AppOrder(){}
	
	public AppOrder(ResultSet sqlResult) throws SQLException{
		this.id = sqlResult.getLong("ID");
		this.idOrder = sqlResult.getString("ID_ORDER");
		this.idContract = sqlResult.getString("ID_CONTRACT");
		this.module = sqlResult.getString("MODULE");
		this.idContractModule = sqlResult.getString("ID_CONTRACT_MODULE");
		this.purchasePrice = sqlResult.getDouble("PURCHASE_PRICE");
		if(sqlResult.getTimestamp("EMISSION_DATE") != null){this.emissionDate = sqlResult.getTimestamp("EMISSION_DATE").getTime();}
		if(sqlResult.getTimestamp("VALUE_DATE") != null){this.valueDate = sqlResult.getTimestamp("VALUE_DATE").getTime();}
		this.practiceStatus = sqlResult.getString("PRACTICE_STATUS");
		this.userAdopter = sqlResult.getString("USER_ADOPTER");
		this.purchaseDescription = sqlResult.getString("PURCHASE_DESCRIPTION");
		this.goodStatus = sqlResult.getString("GOOD_STATUS");
		this.purchaseWarranty =sqlResult.getString("PURCHASE_WARRANTY");
		if(sqlResult.getTimestamp("GOOD_DELIVERY_DATE") != null){this.goodDeliveryDate = sqlResult.getTimestamp("GOOD_DELIVERY_DATE").getTime();}
		this.goodDelPlaceStreet = sqlResult.getString("GOOD_DEL_PLACE_STREET");
		this.goodDelPlaceNumber = sqlResult.getString("GOOD_DEL_PLACE_NUMBER");
		this.goodDelPlaceCity = sqlResult.getString("GOOD_DEL_PLACE_CITY");
		this.goodDelPlaceZip = sqlResult.getString("GOOD_DEL_PLACE_ZIP");
		this.goodDelPlaceProvince = sqlResult.getString("GOOD_DEL_PLACE_PROVINCE");
		this.goodDelPlaceCountry = sqlResult.getString("GOOD_DEL_PLACE_COUNTRY");
		this.goodDeliveryTerms = sqlResult.getString("GOOD_DELIVERY_TERMS");
		this.paymentConditions = sqlResult.getString("PAYMENT_CONDITIONS");
		if(sqlResult.getDate("INSERT_DATE") != null){this.insertDate = sqlResult.getDate("INSERT_DATE").getTime();}
		this.insertUser = sqlResult.getString("INSERT_USER");
		if(sqlResult.getDate("UPDATE_DATE") != null){this.updateDate = sqlResult.getDate("UPDATE_DATE").getTime();}
		this.updateUser = sqlResult.getString("UPDATE_USER");
		this.goodInsPlaceStreet = sqlResult.getString("GOOD_INS_PLACE_STREET");
		this.goodInsPlaceNumber = sqlResult.getString("GOOD_INS_PLACE_NUMBER");
		this.goodInsPlaceCity = sqlResult.getString("GOOD_INS_PLACE_CITY");
		this.goodInsPlaceZip = sqlResult.getString("GOOD_INS_PLACE_ZIP");
		this.goodInsPlaceProvince = sqlResult.getString("GOOD_INS_PLACE_PROVINCE");
		this.goodInsPlaceCountry = sqlResult.getString("GOOD_INS_PLACE_COUNTRY");
		this.metadataAppOrderPojo = new Gson().fromJson(sqlResult.getString("CONFIGURATION"), MetadataAppOrderPojo.class);
		this.ndgProvider = sqlResult.getString("NDG_PROVIDER");
		this.ndgCustomer = sqlResult.getString("NDG_CUSTOMER");
		this.referenceOffice = sqlResult.getString("REFERENCE_OFFICE");
				
		// draft...
		//this.paymentConditionsPojo = new PaymentConditionsPojo();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Double getPurchasePrice() {
		return purchasePrice;
	}

	public void setPurchasePrice(Double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}

	public String getPracticeStatus() {
		return practiceStatus;
	}

	public void setPracticeStatus(String practiceStatus) {
		this.practiceStatus = practiceStatus;
	}

	public String getUserAdopter() {
		return userAdopter;
	}

	public void setUserAdopter(String userAdopter) {
		this.userAdopter = userAdopter;
	}

	public String getPurchaseDescription() {
		return purchaseDescription;
	}

	public void setPurchaseDescription(String purchaseDescription) {
		this.purchaseDescription = purchaseDescription;
	}

	public String getGoodStatus() {
		return goodStatus;
	}

	public void setGoodStatus(String goodStatus) {
		this.goodStatus = goodStatus;
	}

	public String getPurchaseWarranty() {
		return purchaseWarranty;
	}

	public void setPurchaseWarranty(String purchaseWarranty) {
		this.purchaseWarranty = purchaseWarranty;
	}

	
	public String getGoodDelPlaceStreet() {
		return goodDelPlaceStreet;
	}

	public void setGoodDelPlaceStreet(String goodDelPlaceStreet) {
		this.goodDelPlaceStreet = goodDelPlaceStreet;
	}

	public String getGoodDelPlaceNumber() {
		return goodDelPlaceNumber;
	}

	public void setGoodDelPlaceNumber(String goodDelPlaceNumber) {
		this.goodDelPlaceNumber = goodDelPlaceNumber;
	}

	public String getGoodDelPlaceCity() {
		return goodDelPlaceCity;
	}

	public void setGoodDelPlaceCity(String goodDelPlaceCity) {
		this.goodDelPlaceCity = goodDelPlaceCity;
	}

	public String getGoodDelPlaceZip() {
		return goodDelPlaceZip;
	}

	public void setGoodDelPlaceZip(String goodDelPlaceZip) {
		this.goodDelPlaceZip = goodDelPlaceZip;
	}

	public String getGoodDelPlaceProvince() {
		return goodDelPlaceProvince;
	}

	public void setGoodDelPlaceProvince(String goodDelPlaceProvince) {
		this.goodDelPlaceProvince = goodDelPlaceProvince;
	}

	public String getGoodDelPlaceCountry() {
		return goodDelPlaceCountry;
	}

	public void setGoodDelPlaceCountry(String goodDelPlaceCountry) {
		this.goodDelPlaceCountry = goodDelPlaceCountry;
	}

	public String getGoodDeliveryTerms() {
		return goodDeliveryTerms;
	}

	public void setGoodDeliveryTerms(String gooDdeliveryTerms) {
		this.goodDeliveryTerms = gooDdeliveryTerms;
	}

	public String getPaymentConditions() {
		return paymentConditions;
	}

	public void setPaymentConditions(String paymentConditions) {
		this.paymentConditions = paymentConditions;
	}



	public String getGoodInsPlaceStreet() {
		return goodInsPlaceStreet;
	}

	public void setGoodInsPlaceStreet(String goodInsPlaceStreet) {
		this.goodInsPlaceStreet = goodInsPlaceStreet;
	}

	public String getGoodInsPlaceNumber() {
		return goodInsPlaceNumber;
	}

	public void setGoodInsPlaceNumber(String goodInsPlaceNumber) {
		this.goodInsPlaceNumber = goodInsPlaceNumber;
	}

	public String getGoodInsPlaceCity() {
		return goodInsPlaceCity;
	}

	public void setGoodInsPlaceCity(String goodInsPlaceCity) {
		this.goodInsPlaceCity = goodInsPlaceCity;
	}

	public String getGoodInsPlaceZip() {
		return goodInsPlaceZip;
	}

	public void setGoodInsPlaceZip(String goodInsPlaceZip) {
		this.goodInsPlaceZip = goodInsPlaceZip;
	}

	public String getGoodInsPlaceProvince() {
		return goodInsPlaceProvince;
	}

	public void setGoodInsPlaceProvince(String goodInsPlaceProvince) {
		this.goodInsPlaceProvince = goodInsPlaceProvince;
	}

	public String getGoodInsPlaceCountry() {
		return goodInsPlaceCountry;
	}

	public void setGoodInsPlaceCountry(String goodInsPlaceCountry) {
		this.goodInsPlaceCountry = goodInsPlaceCountry;
	}

	public String getIdOrder() {
		return idOrder;
	}

	public void setIdOrder(String idOrder) {
		this.idOrder = idOrder;
	}

	public String getIdContract() {
		return idContract;
	}

	public void setIdContract(String idContract) {
		this.idContract = idContract;
	}

	public Long getEmissionDate() {
		return emissionDate;
	}

	public void setEmissionDate(Long emissionDate) {
		this.emissionDate = emissionDate;
	}

	public Long getGoodDeliveryDate() {
		return goodDeliveryDate;
	}

	public void setGoodDeliveryDate(Long goodDeliveryDate) {
		this.goodDeliveryDate = goodDeliveryDate;
	}

	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	

	

	public Double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}

	public Double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getIdContractModule() {
		return idContractModule;
	}

	public void setIdContractModule(String idContractModule) {
		this.idContractModule = idContractModule;
	}

	public Long getValueDate() {
		return valueDate;
	}

	public void setValueDate(Long valueDate) {
		this.valueDate = valueDate;
	}



	public String getStartEmissionDate() {
		return startEmissionDate;
	}

	public void setStartEmissionDate(String startEmissionDate) {
		this.startEmissionDate = startEmissionDate;
	}

	public String getEndEmissionDate() {
		return endEmissionDate;
	}

	public void setEndEmissionDate(String endEmissionDate) {
		this.endEmissionDate = endEmissionDate;
	}

	public String getStartValueDate() {
		return startValueDate;
	}

	public void setStartValueDate(String startValueDate) {
		this.startValueDate = startValueDate;
	}

	public String getEndValueDate() {
		return endValueDate;
	}

	public void setEndValueDate(String endValueDate) {
		this.endValueDate = endValueDate;
	}

	public boolean isOrderHistory() {
		return orderHistory;
	}

	public void setOrderHistory(boolean orderHistory) {
		this.orderHistory = orderHistory;
	}

	public MetadataAppOrderPojo getMetadataAppOrderPojo() {
		return metadataAppOrderPojo;
	}

	public void setMetadataAppOrderPojo(MetadataAppOrderPojo metadataAppOrderPojo) {
		this.metadataAppOrderPojo = metadataAppOrderPojo;
	}

//	public PaymentConditionsPojo getPaymentConditionsPojo() {
//		return paymentConditionsPojo;
//	}
//
//	public void setPaymentConditionsPojo(PaymentConditionsPojo paymentConditionsPojo) {
//		this.paymentConditionsPojo = paymentConditionsPojo;
//	}

	public String getNdgProvider() {
		return ndgProvider;
	}

	public void setNdgProvider(String ndgProvider) {
		this.ndgProvider = ndgProvider;
	}

	public String getNdgCustomer() {
		return ndgCustomer;
	}

	public void setNdgCustomer(String ndgCustomer) {
		this.ndgCustomer = ndgCustomer;
	}

	public String getReferenceOffice() {
		return referenceOffice;
	}

	public void setReferenceOffice(String referenceOffice) {
		this.referenceOffice = referenceOffice;
	}
	
}
