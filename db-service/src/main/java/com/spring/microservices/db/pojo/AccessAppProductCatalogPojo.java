package com.spring.microservices.db.pojo;

public class AccessAppProductCatalogPojo {

	private Double value;
	private String description;
	
	public AccessAppProductCatalogPojo(){}
	
	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
