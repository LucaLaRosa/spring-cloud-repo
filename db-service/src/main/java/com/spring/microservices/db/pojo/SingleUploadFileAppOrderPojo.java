package com.spring.microservices.db.pojo;

public class SingleUploadFileAppOrderPojo {
	
	private boolean hasError;
    private boolean isSelected;
	private String nameFile;
	
	public String getNameFile() {
		return nameFile;
	}

	public void setNameFile(String nameFile) {
		this.nameFile = nameFile;
	}

	public SingleUploadFileAppOrderPojo(){}


	public boolean isHasError() {
		return hasError;
	}


	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}


	public boolean isSelected() {
		return isSelected;
	}


	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}


	
	
}
