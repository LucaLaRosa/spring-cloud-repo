package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class AppSimulation extends PaginationBaseEntity{	
	private Long id;
	private Long idWfInstance;
	private String status;
	
	private Long idAppVendor;
	private String idAppVendorUser;
	
	private String pivaCustomerUserG;
	private String ndgCustomerUserG;
	private String cfCustomerUserF;
	private String ndgCustomerUserF;
	
	private Long insertTs;
	private String insertUser;
	private Long updateTs;
	private String updateUser;
	private String origin;
	
	private String title; // nome simulazione wf customizzata
	private String metadata;
	
	//transient
	private boolean selected;
		
	public AppSimulation() {}
	
	public AppSimulation(ResultSet sqlResult) throws SQLException {
		this.id = sqlResult.getLong("ID");
		this.idWfInstance = sqlResult.getLong("ID_WF_INSTANCE");
		this.status = sqlResult.getString("STATUS");
		this.idAppVendor = sqlResult.getLong("ID_APP_VENDOR");
		this.idAppVendorUser = sqlResult.getString("ID_APP_VENDOR_USER");
		
		this.pivaCustomerUserG = sqlResult.getString("PIVA_CUSTOMER_USER_G");
		this.cfCustomerUserF = sqlResult.getString("CF_CUSTOMER_USER_F");
		this.ndgCustomerUserF = sqlResult.getString("NDG_CUSTOMER_USER_F");
		this.ndgCustomerUserG = sqlResult.getString("NDG_CUSTOMER_USER_G");
		
		if(sqlResult.getTimestamp("INSERT_TS") != null){this.insertTs = sqlResult.getTimestamp("INSERT_TS").getTime();}
		this.insertUser = sqlResult.getString("INSERT_USER");
		if(sqlResult.getTimestamp("UPDATE_TS") != null){this.updateTs = sqlResult.getTimestamp("UPDATE_TS").getTime();}
		this.updateUser = sqlResult.getString("UPDATE_USER");
		this.origin = sqlResult.getString("ORIGIN");
		this.title = sqlResult.getString("TITLE");
		this.metadata = sqlResult.getString("METADATA");
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdWfInstance() {
		return idWfInstance;
	}

	public void setIdWfInstance(Long idWfInstance) {
		this.idWfInstance = idWfInstance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getIdAppVendor() {
		return idAppVendor;
	}

	public void setIdAppVendor(Long idAppVendor) {
		this.idAppVendor = idAppVendor;
	}

	public String getIdAppVendorUser() {
		return idAppVendorUser;
	}

	public void setIdAppVendorUser(String idAppVendorUser) {
		this.idAppVendorUser = idAppVendorUser;
	}

	public String getPivaCustomerUserG() {
		return pivaCustomerUserG;
	}

	public void setPivaCustomerUserG(String pivaCustomerUserG) {
		this.pivaCustomerUserG = pivaCustomerUserG;
	}

	public String getNdgCustomerUserG() {
		return ndgCustomerUserG;
	}

	public void setNdgCustomerUserG(String ndgCustomerUserG) {
		this.ndgCustomerUserG = ndgCustomerUserG;
	}

	public String getCfCustomerUserF() {
		return cfCustomerUserF;
	}

	public void setCfCustomerUserF(String cfCustomerUserF) {
		this.cfCustomerUserF = cfCustomerUserF;
	}

	public String getNdgCustomerUserF() {
		return ndgCustomerUserF;
	}

	public void setNdgCustomerUserF(String ndgCustomerUserF) {
		this.ndgCustomerUserF = ndgCustomerUserF;
	}

	public Long getInsertTs() {
		return insertTs;
	}

	public void setInsertTs(Long insertTs) {
		this.insertTs = insertTs;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Long getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(Long updateTs) {
		this.updateTs = updateTs;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
}



