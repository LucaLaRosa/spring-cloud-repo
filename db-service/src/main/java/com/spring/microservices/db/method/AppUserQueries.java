package com.spring.microservices.db.method;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spring.microservices.db.config.DB_Connection;
import com.spring.microservices.db.config.DataAccessConfiguration;
import com.spring.microservices.db.entity.AppCustomerLink;
import com.spring.microservices.db.entity.AppCustomerUser;
import com.spring.microservices.db.entity.AppPassword;
import com.spring.microservices.db.entity.AppVendorUser;
import com.spring.microservices.db.method.utils.DynamicQuery;
import com.spring.microservices.db.method.utils.QueryUtilsAppCustomerUser;
import com.spring.microservices.db.method.resources.*;
import com.google.gson.Gson;

@RestController
public class AppUserQueries {
	//private AdvancedLogger logger = new AdvancedLogger(AppUserQueries.class.getName());
	private static final String GET_APP_VENDOR_USER_BY_CREDENTIALS = "app.user.queries.getAppVendorUserByCredentials";
	private static final String UPDATE_APP_VENDOR_USER_STATUS = "app.user.queries.updateAppVendorUserStatus";
	private static final String INSERT_APP_CUSTOMER_USER = "app.customerUser.queries.insertAppCustomerUser";
	private static final String UPDATE_APP_CUSTOMER_USER_F = "app.customerUser.queries.updateAppCustomerUserF";
	private static final String UPDATE_APP_CUSTOMER_USER_G = "app.customerUser.queries.updateAppCustomerUserG";
	private static final String GET_ALL_LINKED_APP_CUSTOMER_USER_BY_PIVA = "app.customerUser.queries.getAllLinkedAppCustomerUserByPiva";
	private static final String GET_ALL_LINKED_APP_CUSTOMER_USER_BY_CF = "app.customerUser.queries.getAllLinkedAppCustomerUserByCf";
	private static final String CHECK_UNIQUE_EMAIL_APP_CUSTOMER_USER = "app.customerUser.queries.checkUniqueEmailAppCustomer";
	private static final String INSERT_APP_CUSTOMER_LINK = "app.customerLink.queries.insertAppCustomerLink";
	private static final String GET_APP_VENDOR_USER_EXIST = "app.user.queries.getAppVendorUserExist";
	private static final String GET_APP_PASSWORD_BY_EMAIL = "app.user.queries.getAppPasswordByEmail";
	private static final String UPDATE_APP_PASSWORD = "app.user.queries.updateAppPassword";
	private static final String UPDATE_RECOVERY_APP_PASSWORD = "app.user.queries.updateRecoveryAppPassword";
	private static final String GET_MATCH_RECOVERY_PASS ="app.user.queries.getMatchRecoveryPass";
	private static final String GET_APP_VENDOR_USER_BY_CREDENTIALS_RECOVERY ="app.user.queries.getAppVendorUserByCredentialsRecovery";
	private static final String GET_MATCH_OLD_PASS = "app.user.queries.getMatchOldPass";
	private static final String GET_MATCH_LAST_PASS ="app.user.queries.getMatchLastPass";
	private static final String UPDATE_APP_PASSWORD_LAST_ACCESS ="app.user.queries.updateAppPasswordLastAccess";
	
	
	public AppVendorUser getAppVendorUserByCredentials(AppVendorUser input){
		Connection conn = null;
		try {
			conn =  DB_Connection.openConnection();

			String statement = new Methods().getQuery(GET_APP_VENDOR_USER_BY_CREDENTIALS);

			PreparedStatement stmt = conn.prepareStatement(statement);
			stmt.setString(1, input.getPassword());
			stmt.setString(2, input.getEmail().toLowerCase());
			ResultSet resultQuery = stmt.executeQuery();
			resultQuery.next();
			return new AppVendorUser(resultQuery);
		} catch (Exception e) {
			//logger.severe(e);					
			return null;
		}
		finally{
			DB_Connection.closeConnection(conn);
		}
	}
	
	//login by credentials recovery  getAppVendorUserByCredentialsRecovery
	public AppVendorUser getAppVendorUserByCredentialsRecovery(AppVendorUser input){
		Connection conn = null;
		try {
			conn =  DB_Connection.openConnection();

			String statement = new Methods().getQuery(GET_APP_VENDOR_USER_BY_CREDENTIALS_RECOVERY);

			PreparedStatement stmt = conn.prepareStatement(statement);
			stmt.setString(1, input.getPassword());
			stmt.setString(2, input.getEmail().toLowerCase());
			ResultSet resultQuery = stmt.executeQuery();
			resultQuery.next();
			return new AppVendorUser(resultQuery);
		} catch (Exception e) {
			//logger.severe(e);					
			return null;
		}
		finally{
			DB_Connection.closeConnection(conn);
		}
	}
	//fine login bt credentials recovery

	//GET_APP_PASSWORD_BY_EMAIL
	public AppPassword getAppPasswordByEmail(AppPassword input){
		Connection conn = null;
		try {
			conn =  DB_Connection.openConnection();

			String statement = new Methods().getQuery(GET_APP_PASSWORD_BY_EMAIL);

			PreparedStatement stmt = conn.prepareStatement(statement);
			stmt.setString(1, input.getEmail().toLowerCase());

			ResultSet resultQuery = stmt.executeQuery();
			resultQuery.next();
			return new AppPassword(resultQuery);
		} catch (Exception e) {
			//logger.severe(e);					
			return null;
		}
		finally{
			DB_Connection.closeConnection(conn);
		}
	}

	//fine GET_APP_PASSWORD_BY_EMAIL

	//GET_APP_PASSWORD_BY_EMAIL
		public AppPassword getAppPasswordByEmailStringInput(String input){
			Connection conn = null;
			try {
				conn =  DB_Connection.openConnection();

				String statement = new Methods().getQuery(GET_APP_PASSWORD_BY_EMAIL);

				PreparedStatement stmt = conn.prepareStatement(statement);
				stmt.setString(1, input.toLowerCase());

				ResultSet resultQuery = stmt.executeQuery();
				resultQuery.next();
				return new AppPassword(resultQuery);
			} catch (Exception e) {
			//	logger.severe(e);					
				return null;
			}
			finally{
				DB_Connection.closeConnection(conn);
			}
		}

		//fine GET_APP_PASSWORD_BY_EMAIL


	
	//getMatchRecoveryPass per il recupero della password
	public boolean getMatchRecoveryPass(AppPassword input){
		Connection conn = null;
		try {
			conn =  DB_Connection.openConnection();

			String statement = new Methods().getQuery(GET_MATCH_RECOVERY_PASS);

			PreparedStatement stmt = conn.prepareStatement(statement);
			stmt.setString(1, input.getEmail().toLowerCase());
			stmt.setString(2, input.getRecovery());

			ResultSet resultQuery = stmt.executeQuery();
			resultQuery.next();
			return true;
		} catch (Exception e) {
			//logger.severe(e);					
			return false;
		}
		finally{
			DB_Connection.closeConnection(conn);
		}
	}
	//fine
	
	//getMatchOldPass per il cambio password normale
	public boolean getMatchOldPass(AppPassword input){
		Connection conn = null;
		try {
			conn =  DB_Connection.openConnection();

			String statement = new Methods().getQuery(GET_MATCH_OLD_PASS);

			PreparedStatement stmt = conn.prepareStatement(statement);
			stmt.setString(1, input.getEmail().toLowerCase());
			stmt.setString(2, input.getRecovery());

			ResultSet resultQuery = stmt.executeQuery();
			return resultQuery.next();
		} catch (Exception e) {
			//logger.severe(e);					
			return false;
		}
		finally{
			DB_Connection.closeConnection(conn);
		}
	}
	//fine
	
	//
	//update APP_PASSWORD_LAST_ACCESS
		public boolean updateAppPasswordLastAccess(String input){
			Connection conn = null;
			try {
				conn =  DB_Connection.openConnection();

				String filterValue = (input.toLowerCase());
				String statement = new Methods().getQuery(UPDATE_APP_PASSWORD_LAST_ACCESS);

				PreparedStatement stmt = conn.prepareStatement(statement);

				stmt.setString(1, filterValue);
				int affectedRows = stmt.executeUpdate();
				if (affectedRows > 0) {
					return true;
				} else {
					throw new SQLException("Update updateAppPassword failed, no rows affected.");
				}
			} catch (Exception e) {
				//logger.severe(new StacktraceConverter().t2str(e));
				return false;
			}
			finally{
				DB_Connection.closeConnection(conn);
			}

		}

		//fine updateL
	
	
	
	
	
	
	//update APP_PASSWORD
	public boolean updateAppPassword(AppPassword input){
		Long timeNow = new Date().getTime();
		Connection conn = null;
		try {
			conn =  DB_Connection.openConnection();

			String filterValue = (input.getEmail().toLowerCase());
			String statement = new Methods().getQuery(UPDATE_APP_PASSWORD);

			PreparedStatement stmt = conn.prepareStatement(statement);

			stmt.setString(1,  (input.getEmail().toLowerCase()));
			stmt.setDate(2, new java.sql.Date(timeNow));
			stmt.setDate(3,  new java.sql.Date(timeNow));
			stmt.setString(4,  (input.getPassword()));
			stmt.setString(5,  (input.getRecovery()));
			stmt.setString(6,  (input.getOldPassword()));
			stmt.setString(7,  (filterValue));
			int affectedRows = stmt.executeUpdate();
			if (affectedRows > 0) {
				return true;
			} else {
				throw new SQLException("Update updateAppPassword failed, no rows affected.");
			}
		} catch (Exception e) {
			//logger.severe(new StacktraceConverter().t2str(e));
			return false;
		}
		finally{
			DB_Connection.closeConnection(conn);
		}

	}

	//fine update APP_PASSWORD_BY_EMAIL
	
	
	
	public AppPassword getMatchLastPassword(AppPassword input){
		Connection conn = null;
		try {
			conn =  DB_Connection.openConnection();

			String statement = new Methods().getQuery(GET_MATCH_LAST_PASS);
			PreparedStatement stmt = conn.prepareStatement(statement);
			stmt.setString(1, input.getPassword());
			stmt.setString(2, input.getEmail().toLowerCase());
			stmt.setString(3, input.getPassword());

			ResultSet resultQuery = stmt.executeQuery();
			while (resultQuery.next()) { 
				return new AppPassword(resultQuery);
			}
			return null;
		} catch (Exception e) {
		//	logger.severe(e);					
			return null;
		}
		finally{
			DB_Connection.closeConnection(conn);
		}
	}
	
	public boolean updateRecoveryAppPassword(String inputEmail , String inputRecovery){
		Long timeNow = new Date().getTime();
		Connection conn = null;
		try {
			conn =  DB_Connection.openConnection();

			String filterValue = (inputEmail.toLowerCase());
			String statement = new Methods().getQuery(UPDATE_RECOVERY_APP_PASSWORD);



			PreparedStatement stmt = conn.prepareStatement(statement);
			stmt.setDate(1, new java.sql.Date(timeNow));
			stmt.setDate(2, new java.sql.Date(timeNow));
			stmt.setString(3,  (inputRecovery));
			stmt.setString(4,  (filterValue));

			int affectedRows = stmt.executeUpdate();
			if (affectedRows > 0) {
				return true;
			} else {
				throw new SQLException("Update updateAppPassword failed, no rows affected.");
			}
		} catch (Exception e) {
			//logger.severe(new StacktraceConverter().t2str(e));
			return false;
		}
		finally{
			DB_Connection.closeConnection(conn);
		}

	}
	
	//Update APP_PASSWORD_BY_EMAIL
	
	
	
	//fine Update APP_PASSWORD_BY_EMAIL
	
	

	//query eseguita nel recupera password

    @RequestMapping(method = RequestMethod.GET, value = "db/appVendorUser/{inputEmail:.+}")
	public AppVendorUser getAppVendorUserExist(@PathVariable("inputEmail") String inputEmail) throws SQLException{
		Connection conn = null;
		try {
			//----
			//conn = DataAccessConfiguration.datasource().getConnection();
	        DataSource ds = new DataSource();
	        ds.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	        ds.setUrl("jdbc:oracle:thin:@//ALBUTFCN.DB.ITACASERVICE.IT:1521/ALBPVS1");
	        ds.setUsername("ALBA_PV");
	        ds.setPassword("AcN01alB4l3s1ng10");
	        ds.setInitialSize(5);
	        ds.setMaxActive(10);
	        ds.setMaxIdle(5);
	        ds.setMinIdle(2);
	        conn = ds.getConnection();
	        String statement = new Methods().getQuery(GET_APP_VENDOR_USER_EXIST);

			PreparedStatement stmt = conn.prepareStatement(statement);
			stmt.setString(1, inputEmail.toLowerCase());

			ResultSet resultQuery = stmt.executeQuery();
			if(resultQuery.next()){
				return new AppVendorUser(resultQuery);
			}
			return null;
		} catch (Exception e) {
							
			return null;
		}
		finally{
			
		}
	}
	//fine query eseguita nel recupera password


	//query cambio status in APP_VENDOR_USER dopo recupera password
	//devo passarmi l'oggetto AppCustomer USER
	public boolean updateAppVendorUserStatus(AppVendorUser input) {
		Long timeNow = new Date().getTime();
		Connection conn = null;
		try {
			conn =  DB_Connection.openConnection();

			String statement = new Methods().getQuery(UPDATE_APP_VENDOR_USER_STATUS);
			PreparedStatement stmt = conn.prepareStatement(statement);
			stmt.setDate(1, new java.sql.Date(timeNow));
			stmt.setString(2, input.getUpdateUser());
			stmt.setString(3, Methods.upp(input.getStatus()));
			stmt.setInt(4,input.getContatore());
			stmt.setString(5, input.getEmail());
		
			int affectedRows = stmt.executeUpdate();
			if (affectedRows > 0) {
				return true;
			} else {
				throw new SQLException("Update updateAppVendorUser failed, no rows affected.");
			}
		} catch (Exception e) {
			//logger.severe(new StacktraceConverter().t2str(e));
			return false;
		}
		finally{
			DB_Connection.closeConnection(conn);
		}
	}

	//fine cambio status in APP_VENDOR_USER dopo recupera password


	
}
