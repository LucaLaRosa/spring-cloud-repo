package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AppCustomerLink {	
	private String cf;
	private String piva;
	private Long insertDate;
	private String insertUser;
	private boolean disabled;
	private boolean deleted;

	public AppCustomerLink() {}

	public AppCustomerLink(ResultSet sqlResult) throws SQLException {
		this.cf = sqlResult.getString("CF");
		this.piva = sqlResult.getString("PIVA");
		if(sqlResult.getDate("INSERT_DATE") != null){this.insertDate = sqlResult.getDate("INSERT_DATE").getTime();}
		this.insertUser = sqlResult.getString("INSERT_USER");
		this.disabled = sqlResult.getBoolean("DISABLED");
		this.deleted = sqlResult.getBoolean("DELETED");
	}

	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getPiva() {
		return piva;
	}

	public void setPiva(String piva) {
		this.piva = piva;
	}

	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
}



