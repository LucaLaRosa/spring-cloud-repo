package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.alba.pv.utils.Params;

public class UniTypeDomain {	
	private String tableName;
	private String columnName;
	private String value;
	private String name;
	private String description;
	private Long validityDateFrom;
	private Long validityDateTo;
	private Long insertDate;
	private String insertUser;
	private Long updateDate;
	private String updateUser;
	private boolean disabled;
	private boolean deleted;

	public UniTypeDomain() {}

	public UniTypeDomain(ResultSet sqlResult) {
		try {
			this.tableName = sqlResult.getString("TABLE_NAME");
			this.columnName = sqlResult.getString("COLUMN_NAME");
			this.value = sqlResult.getString("VALUE");
			this.description = sqlResult.getString("DESCRIPTION");
			if(sqlResult.getDate("VALIDITY_DATE_FROM") != null){this.validityDateFrom = sqlResult.getDate("VALIDITY_DATE_FROM").getTime();}
			if(sqlResult.getDate("VALIDITY_DATE_TO") != null){this.validityDateTo = sqlResult.getDate("VALIDITY_DATE_TO").getTime();}
			if(sqlResult.getDate("INSERT_DATE") != null){this.insertDate = sqlResult.getDate("INSERT_DATE").getTime();}
			this.insertUser = sqlResult.getString("INSERT_USER");
			if(sqlResult.getDate("UPDATE_DATE") != null){this.updateDate = sqlResult.getDate("UPDATE_DATE").getTime();}
			this.updateUser = sqlResult.getString("UPDATE_USER");
			this.disabled = sqlResult.getBoolean("DISABLED");
			this.deleted = sqlResult.getBoolean("DELETED");
		} catch (SQLException ex) {
			Logger.getLogger(UniTypeDomain.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

//	public UniTypeDomain(ResultSet sqlResult, String frontendPojo) {
//		try {
//			if(Params.LIGHTER_FRONTEND_POJO.equals(frontendPojo)){
//				this.tableName = sqlResult.getString("TABLE_NAME");
//				this.columnName = sqlResult.getString("COLUMN_NAME");
//				this.value = sqlResult.getString("VALUE");
//				this.name = sqlResult.getString("NAME");
//				this.description = sqlResult.getString("DESCRIPTION");
//			}
//		} catch (SQLException ex) {
//			Logger.getLogger(UniTypeDomain.class.getName()).log(Level.SEVERE, null, ex);
//		}
//	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getValidityDateFrom() {
		return validityDateFrom;
	}

	public void setValidityDateFrom(Long validityDateFrom) {
		this.validityDateFrom = validityDateFrom;
	}

	public Long getValidityDateTo() {
		return validityDateTo;
	}

	public void setValidityDateTo(Long validityDateTo) {
		this.validityDateTo = validityDateTo;
	}

	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	
	
}
