package com.spring.microservices.db.method.utils;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.spring.microservices.db.entity.AppCustomerUser;

public class QueryUtilsAppCustomerUser {

	public static List<DynamicQuery> createDynamicStatementByFilterAppCustomerUser(AppCustomerUser filter) {
		if(filter != null){
			List<DynamicQuery> oL = new ArrayList<DynamicQuery>();
			if(filter.getName() != null
					&& !filter.getName().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" upper(NAME) LIKE upper(?) ", "%"+filter.getName()+"%"));
			}
			if(filter.getMergedRagSocSurname() != null
					&& !filter.getMergedRagSocSurname().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" ( upper(SURNAME) LIKE upper(?) ", "%"+filter.getMergedRagSocSurname()+"%"));
				oL.add(new DynamicQuery(" OR upper(RAG_SOC) LIKE upper(?) ) ", "%"+filter.getMergedRagSocSurname()+"%"));
			}
			if(filter.getMergedPivaCf() != null
					&& !filter.getMergedPivaCf().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" ( upper(PIVA) = upper(?) ", filter.getMergedPivaCf()));
				oL.add(new DynamicQuery(" OR upper(CF) = upper(?) ) ", filter.getMergedPivaCf()));
			}
			if(filter.getNdg() != null
					&& !filter.getNdg().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" upper(NDG) LIKE upper(?) ", "%"+filter.getNdg()+"%"));
			}
			if(filter.getPiva() != null
					&& !filter.getPiva().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" upper(PIVA) LIKE upper(?) ", "%"+filter.getPiva()+"%"));
			}
			if(filter.getEqualPiva() != null
					&& !filter.getEqualPiva().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" upper(PIVA) = upper(?) ", filter.getEqualPiva()));
			}
			if(filter.getRagSoc() != null
					&& !filter.getRagSoc().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" upper(RAG_SOC) LIKE upper(?) ", "%"+filter.getRagSoc()+"%"));
			}
			if(filter.getCf() != null
					&& !filter.getCf().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" upper(CF) LIKE upper(?) ", "%"+filter.getCf()+"%"));
			}
			if(filter.getEqualCf() != null
					&& !filter.getEqualCf().trim().equals("")){
				oL.add(new DynamicQuery(addAnd(oL.size())+" upper(CF) = upper(?) ", filter.getEqualCf()));
			}
			return oL;
		}
		return null;
	}

	private static String addAnd(int size) {
		if(size > 0){
			return " AND ";
		}
		return "";
	}

	public static void setPreparedStatement(PreparedStatement stmt, int index ,DynamicQuery dynamicQuery) throws SQLException {
		if(DynamicQuery.STRING_TYPE.equals(dynamicQuery.getQueryItemInputType())){
			stmt.setString(index, (String)dynamicQuery.getQueryItemValue());
		} else if(DynamicQuery.DOUBLE_TYPE.equals(dynamicQuery.getQueryItemInputType())){
			stmt.setDouble(index, (Double)dynamicQuery.getQueryItemValue());
		} else if(DynamicQuery.DATE_TYPE.equals(dynamicQuery.getQueryItemInputType())){
			stmt.setTimestamp(index, new Timestamp((Long)dynamicQuery.getQueryItemValue()));
		}
	}
}
