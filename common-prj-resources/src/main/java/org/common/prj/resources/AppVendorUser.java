package  org.common.prj.resources;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

//import com.alba.pv.services.rest.AuthService;
//import com.alba.pv.utils.AdvancedLogger;

public class AppVendorUser {
	private String email;
	private Long idAppVendor;
	private Long idAppGroup;
	private String type;
	private Long validityDateFrom;
	private Long validityDateTo;
	private Long insertDate;
	private String insertUser;
	private Long updateDate;
	private String updateUser;
	private boolean disabled;
	private boolean deleted;
	private String name;
	private String surname;
	private String status;
	private int contatore;
	private String description;
	
	//transient
	private AppVendor vendor;
	private String password;
		
	public AppVendorUser() {}
	
	public AppVendorUser(ResultSet sqlResult) throws SQLException {
		this.email = sqlResult.getString("EMAIL");
		this.idAppVendor = sqlResult.getLong("ID_APP_VENDOR");
		this.idAppGroup = sqlResult.getLong("ID_APP_GROUP");
		this.type = sqlResult.getString("TYPE");
		if(sqlResult.getDate("VALIDITY_DATE_FROM") != null){this.validityDateFrom = sqlResult.getDate("VALIDITY_DATE_FROM").getTime();}
		if(sqlResult.getDate("VALIDITY_DATE_TO") != null){this.validityDateTo = sqlResult.getDate("VALIDITY_DATE_TO").getTime();}
		if(sqlResult.getDate("INSERT_DATE") != null){this.insertDate = sqlResult.getDate("INSERT_DATE").getTime();}
		this.insertUser = sqlResult.getString("INSERT_USER");
		if(sqlResult.getDate("UPDATE_DATE") != null){this.updateDate = sqlResult.getDate("UPDATE_DATE").getTime();}
		this.updateUser = sqlResult.getString("UPDATE_USER");
		this.disabled = sqlResult.getBoolean("DISABLED");
		this.deleted = sqlResult.getBoolean("DELETED");
		this.name = sqlResult.getString("NAME");
		this.surname = sqlResult.getString("SURNAME");
		this.status = sqlResult.getString("STATUS");
		this.contatore = sqlResult.getInt("CONTATORE");
		this.description = sqlResult.getString("DESCRIPTION");
		
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Long getIdAppVendor() {
		return idAppVendor;
	}

	public void setIdAppVendor(Long idAppVendor) {
		this.idAppVendor = idAppVendor;
	}

	public Long getIdAppGroup() {
		return idAppGroup;
	}

	public void setIdAppGroup(Long idAppGroup) {
		this.idAppGroup = idAppGroup;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getValidityDateFrom() {
		return validityDateFrom;
	}

	public void setValidityDateFrom(Long validityDateFrom) {
		this.validityDateFrom = validityDateFrom;
	}

	public Long getValidityDateTo() {
		return validityDateTo;
	}

	public void setValidityDateTo(Long validityDateTo) {
		this.validityDateTo = validityDateTo;
	}

	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public AppVendor getVendor() {
		return vendor;
	}

	public void setVendor(AppVendor vendor) {
		this.vendor = vendor;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getContatore() {
		return contatore;
	}

	public void setContatore(int contatore) {
		this.contatore = contatore;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	
}



