package com.spring.microservices.db.pojo;

import java.util.ArrayList;

public class MetadataAppOrderPojo {
	private ArrayList<ConfigurationUploadFileAppOrderPojo> uploadedfile;

	public MetadataAppOrderPojo(){}

	public ArrayList<ConfigurationUploadFileAppOrderPojo> getUploadedfile() {
		return uploadedfile;
	}

	public void setUploadedfile(ArrayList<ConfigurationUploadFileAppOrderPojo> uploadedfile) {
		this.uploadedfile = uploadedfile;
	}

	
	
}
