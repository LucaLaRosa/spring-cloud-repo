package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AppCustomerUser {	
	private String email;
	private String password;
	private String cf;
	private String type;
	private String name;
	private String surname;
	private String piva;
	private String ndg;
	private String phone;
	private boolean emailConf;
	private boolean smsConf;
	private Long validityDateFrom;
	private Long validityDateTo;
	private Long insertDate;
	private String insertUser;
	private Long updateDate;
	private String updateUser;
	private boolean disabled;
	private boolean deleted;
	private String legalForm;
	private String codRae;
	private String codAteco;
	private boolean prospect;
	private String ragSoc;

	//transient 
	private int offset;
	private int limit;
	private String mergedRagSocSurname;
	private String mergedPivaCf;
	private AppCustomerLink link; //link between customer F and customer G
	private boolean selected;
	private boolean unknown;
	private boolean fromCerved;
	private int pageIndex;
	private int totalCount;
	private int totalPages;
	private String orderColumnName;
	private String orderType; //DESC ASC...
	private String equalCf;
	private String equalPiva;

	public AppCustomerUser() {}

	public AppCustomerUser(ResultSet sqlResult) throws SQLException {
		this.email = sqlResult.getString("EMAIL");
		this.cf = sqlResult.getString("CF");
		this.type = sqlResult.getString("TYPE");
		this.name = sqlResult.getString("NAME");
		this.surname = sqlResult.getString("SURNAME");
		this.piva = sqlResult.getString("PIVA");
		this.ndg = sqlResult.getString("NDG");
		this.phone = sqlResult.getString("PHONE");
		this.emailConf = sqlResult.getBoolean("EMAIL_CONF");
		this.smsConf = sqlResult.getBoolean("SMS_CONF");
		if(sqlResult.getDate("VALIDITY_DATE_FROM") != null){this.validityDateFrom = sqlResult.getDate("VALIDITY_DATE_FROM").getTime();}
		if(sqlResult.getDate("VALIDITY_DATE_TO") != null){this.validityDateTo = sqlResult.getDate("VALIDITY_DATE_TO").getTime();}
		if(sqlResult.getDate("INSERT_DATE") != null){this.insertDate = sqlResult.getDate("INSERT_DATE").getTime();}
		this.insertUser = sqlResult.getString("INSERT_USER");
		if(sqlResult.getDate("UPDATE_DATE") != null){this.updateDate = sqlResult.getDate("UPDATE_DATE").getTime();}
		this.updateUser = sqlResult.getString("UPDATE_USER");
		this.disabled = sqlResult.getBoolean("DISABLED");
		this.deleted = sqlResult.getBoolean("DELETED");
		this.legalForm = sqlResult.getString("LEGAL_FORM");
		this.codRae = sqlResult.getString("COD_RAE");
		this.codAteco = sqlResult.getString("COD_ATECO");
		this.prospect = sqlResult.getBoolean("PROSPECT");
		this.ragSoc = sqlResult.getString("RAG_SOC");
		this.mergedRagSocSurname = getMergedValue(this.type, this.surname, this.ragSoc);
	}

	private String getMergedValue(String typeVal, String surnameVal, String ragSocVal) {
		if("G".equals(typeVal)){
			return ragSocVal;
		} else if("F".equals(typeVal)){
			return surnameVal;
		}
		return null;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getPiva() {
		return piva;
	}

	public void setPiva(String piva) {
		this.piva = piva;
	}

	public String getNdg() {
		return ndg;
	}

	public void setNdg(String ndg) {
		this.ndg = ndg;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean isEmailConf() {
		return emailConf;
	}

	public void setEmailConf(boolean emailConf) {
		this.emailConf = emailConf;
	}

	public boolean isSmsConf() {
		return smsConf;
	}

	public void setSmsConf(boolean smsConf) {
		this.smsConf = smsConf;
	}

	public Long getValidityDateFrom() {
		return validityDateFrom;
	}

	public void setValidityDateFrom(Long validityDateFrom) {
		this.validityDateFrom = validityDateFrom;
	}

	public Long getValidityDateTo() {
		return validityDateTo;
	}

	public void setValidityDateTo(Long validityDateTo) {
		this.validityDateTo = validityDateTo;
	}

	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public String getLegalForm() {
		return legalForm;
	}

	public void setLegalForm(String legalForm) {
		this.legalForm = legalForm;
	}

	public String getCodRae() {
		return codRae;
	}

	public void setCodRae(String codRae) {
		this.codRae = codRae;
	}

	public String getCodAteco() {
		return codAteco;
	}

	public void setCodAteco(String codAteco) {
		this.codAteco = codAteco;
	}

	public boolean isProspect() {
		return prospect;
	}

	public void setProspect(boolean prospect) {
		this.prospect = prospect;
	}

	public String getRagSoc() {
		return ragSoc;
	}

	public void setRagSoc(String ragSoc) {
		this.ragSoc = ragSoc;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getMergedRagSocSurname() {
		return mergedRagSocSurname;
	}

	public void setMergedRagSocSurname(String mergedRagSocSurname) {
		this.mergedRagSocSurname = mergedRagSocSurname;
	}

	public AppCustomerLink getLink() {
		return link;
	}

	public void setLink(AppCustomerLink link) {
		this.link = link;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public int getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public String getOrderColumnName() {
		return orderColumnName;
	}

	public void setOrderColumnName(String orderColumnName) {
		this.orderColumnName = orderColumnName;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public String getEqualCf() {
		return equalCf;
	}

	public void setEqualCf(String equalCf) {
		this.equalCf = equalCf;
	}

	public String getEqualPiva() {
		return equalPiva;
	}

	public void setEqualPiva(String equalPiva) {
		this.equalPiva = equalPiva;
	}

	public boolean isUnknown() {
		return unknown;
	}

	public void setUnknown(boolean unknown) {
		this.unknown = unknown;
	}

	public String getMergedPivaCf() {
		return mergedPivaCf;
	}

	public void setMergedPivaCf(String mergedPivaCf) {
		this.mergedPivaCf = mergedPivaCf;
	}

	public boolean isFromCerved() {
		return fromCerved;
	}

	public void setFromCerved(boolean fromCerved) {
		this.fromCerved = fromCerved;
	}
	
	
	
}



