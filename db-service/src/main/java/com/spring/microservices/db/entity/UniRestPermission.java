package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UniRestPermission {
	private String id;
	private String functionality;
	private List<String> channelList;
	private List<String> userTypeList;
	private boolean disabled;
	private String title;
	private String description;
	
	public UniRestPermission(ResultSet sqlResult) throws SQLException {
		this.id = sqlResult.getString("ID");
		this.functionality = sqlResult.getString("FUNCTIONALITY");
		this.channelList = new Gson().fromJson(sqlResult.getString("CHANNEL_LIST"), new TypeToken<List<String>>(){}.getType());
		this.userTypeList = new Gson().fromJson(sqlResult.getString("USER_TYPE_LIST"), new TypeToken<List<String>>(){}.getType());		
		this.disabled = sqlResult.getBoolean("DISABLED");
		this.title = sqlResult.getString("TITLE");
		this.description = sqlResult.getString("DESCRIPTION");
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFunctionality() {
		return functionality;
	}

	public void setFunctionality(String functionality) {
		this.functionality = functionality;
	}

	public List<String> getChannelList() {
		return channelList;
	}

	public void setChannelList(List<String> channelList) {
		this.channelList = channelList;
	}

	public List<String> getUserTypeList() {
		return userTypeList;
	}

	public void setUserTypeList(List<String> userTypeList) {
		this.userTypeList = userTypeList;
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
