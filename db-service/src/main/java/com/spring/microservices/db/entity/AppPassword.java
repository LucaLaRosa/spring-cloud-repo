package  com.spring.microservices.db.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

//import com.alba.pv.services.config.TopStandardRestFilter;
//import com.alba.pv.utils.AdvancedLogger;


public class AppPassword {
	private String email;
	private Long insertDate;
	private String insertUser;
	private Long updateDate;
	private String updateUser;
	private String password;
	private String recovery;
	public String oldPassword;
	public String hashedPassword;
	public Long lastAccess;
	//transient
	private String confirmedPassword;
	
	
	
	public AppPassword() {}
	
	public AppPassword(ResultSet sqlResult) throws SQLException {
		this.email = sqlResult.getString("EMAIL");
		if(sqlResult.getDate("INSERT_DATE") != null){this.insertDate = sqlResult.getDate("INSERT_DATE").getTime();}
		this.insertUser = sqlResult.getString("INSERT_USER");
		if(sqlResult.getDate("UPDATE_DATE") != null){this.updateDate = sqlResult.getDate("UPDATE_DATE").getTime();}
		this.updateUser = sqlResult.getString("UPDATE_USER");
		this.password = sqlResult.getString("PASSWORD");
		this.recovery = sqlResult.getString("RECOVERY");
		this.oldPassword = sqlResult.getString("OLD_PASSWORD");
		try {
			this.hashedPassword = sqlResult.getString("HASHED_PASSWORD");
		} catch (Exception e) {
//			AdvancedLogger logger = new AdvancedLogger(AppPassword.class.getName());
//			logger.info("Column HASHED_PASSWORD not necessary");
		}
		this.lastAccess = sqlResult.getTimestamp("LAST_ACCESS").getTime();

	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Long insertDate) {
		this.insertDate = insertDate;
	}

	public String getInsertUser() {
		return insertUser;
	}

	public void setInsertUser(String insertUser) {
		this.insertUser = insertUser;
	}

	public Long getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Long updateDate) {
		this.updateDate = updateDate;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRecovery() {
		return recovery;
	}

	public void setRecovery(String recovery) {
		this.recovery = recovery;
	}

	public String getConfirmedPassword() {
		return confirmedPassword;
	}

	public void setConfirmedPassword(String confirmedPassword) {
		this.confirmedPassword = confirmedPassword;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public Long getLastAccess() {
		return lastAccess;
	}

	public void setLastAccess(Long lastAccess) {
		this.lastAccess = lastAccess;
	}

	
	
	
}
