package com.spring.microservices.db.pojo;

public class SpecServAppProductCatalogPojo {

	private Double value;
	private String description;
	private String serviceType;
	
	
	public SpecServAppProductCatalogPojo(){}


	public Double getValue() {
		return value;
	}


	public void setValue(Double value) {
		this.value = value;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getServiceType() {
		return serviceType;
	}


	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}
	
	
}
